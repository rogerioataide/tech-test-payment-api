﻿namespace tech_test_payment_api.Enum
{
    public enum eStatusVenda
    {
        AguardandoPagamento = 0,
        PagamentoAprovado = 1,
        Cancelado = 2,
        EnviadoTransportadora = 3,
        Entregue = 4
    }
}
