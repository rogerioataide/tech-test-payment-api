﻿using AutoMapper;
using tech_test_payment_api.Cache;
using tech_test_payment_api.Enum;
using tech_test_payment_api.Interfaces;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Repository.Exceptions;

namespace tech_test_payment_api.Services
{
    public class VendaService : IVendaService
    {
        private IVendaRepository _vendaRepository;
        private IMapper _mapper;

        public VendaService(IVendaRepository vendaRepository, IMapper mapper)
        {
            _vendaRepository = vendaRepository;
            _mapper = mapper;
        }

        public object GetVenda(string id)
        {
            var retorno = _vendaRepository.GetVenda(id);

            return retorno;
        }

        public void RegistrarVenda(RegistrarVendaParams registrarVendaParams)
        {
            var parametros = new Venda();
            parametros.Vendedor = new Vendedor();

            parametros = _mapper.Map<RegistrarVendaParams, Venda>(registrarVendaParams);

            var vendaExiste = _vendaRepository.VendaExiste(parametros.Id);

            if (vendaExiste)
            {
                throw new VendaJaCadastradaException($"Já existe uma venda salva com o id {registrarVendaParams.Id}!");
            }

            _vendaRepository.SetVenda(parametros);
        }

        public void AtualizarVenda(AtualizarVendaParams parametrosAtualizarVenda)
        {
            var venda = _vendaRepository.GetVenda(parametrosAtualizarVenda.Id);
            ValidarVendaParaAtualizacao(venda, parametrosAtualizarVenda);

            var atualizarVenda = ValidaStatusVenda(venda, parametrosAtualizarVenda);

            if (atualizarVenda) 
            {
                venda.Status = parametrosAtualizarVenda.Status;
                _vendaRepository.SetVenda(venda);
            }
        }

        private void ValidarVendaParaAtualizacao(Venda objetoVenda, AtualizarVendaParams parametrosAtualizarVenda)
        {
            if (objetoVenda == null)
            {
                throw new VendaNaoEncontradaException("Venda não localizada!");
            }

            if (objetoVenda.Status == parametrosAtualizarVenda.Status)
            {
                throw new StatusVendaInvalidoException($"Venda já está com o status {parametrosAtualizarVenda.Status}!");
            }
        }

        private bool ValidaStatusVenda(Venda venda, AtualizarVendaParams parametrosAtualizarVenda)
        {
            string erro = string.Empty;
            
            switch (venda.Status)
            {
                case (eStatusVenda.AguardandoPagamento):
                    if (parametrosAtualizarVenda.Status != eStatusVenda.PagamentoAprovado && parametrosAtualizarVenda.Status != eStatusVenda.Cancelado)
                    {
                        erro = $"Só é permitido alterar status de 'Aguardando Pagamento' para 'Pagamento Aprovado' ou 'Cancelado'!";
                    }
                    break;
                case (eStatusVenda.PagamentoAprovado):
                    if (parametrosAtualizarVenda.Status != eStatusVenda.EnviadoTransportadora && parametrosAtualizarVenda.Status != eStatusVenda.Cancelado)
                    {
                        erro = $"Só é permitido alterar status de 'Pagamento Aprovado' para 'Enviado Transportadora' ou 'Cancelado'!";
                    }
                    break;
                case (eStatusVenda.EnviadoTransportadora):
                    if (parametrosAtualizarVenda.Status != eStatusVenda.Entregue)
                    {
                        erro = "Só é permitido alterar status de 'Enviado para Transportadora' para 'Entregue'!";
                    }
                    break;
                default:
                    throw new StatusVendaInvalidoException($"Essa atualização de status não é permitida!");
            }

            if (string.IsNullOrEmpty(erro)) 
            {
                return true;
            }

            throw new StatusVendaInvalidoException(erro);
        }

    }
}
