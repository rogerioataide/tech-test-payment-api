﻿using tech_test_payment_api.Cache;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class VendedorService
    {
        private VendedorRepository vendedorRepository = new VendedorRepository();

        public VendedorService(VendedorRepository _vendedorRepository)
        {
            _vendedorRepository = vendedorRepository;
        }

        public void CadastrarVendedor(Vendedor parametros)
        {
            VendedorRepository vendedorRepository = new VendedorRepository();

            DateTimeOffset expiration = DateTimeOffset.Now.AddMinutes(10);
            vendedorRepository.SetItemVendedor(parametros, expiration);
        }
    }
}
