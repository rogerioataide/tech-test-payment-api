﻿using tech_test_payment_api.Cache;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class ProdutoService
    {
        private ProdutoRepository produtoRepository = new ProdutoRepository();

        public ProdutoService(ProdutoRepository _produtoRepository)
        {
            _produtoRepository = produtoRepository;
        }

        public void CadastrarProduto(Produto parametros)
        {
            ProdutoRepository produtoRepository = new ProdutoRepository();

            DateTimeOffset expiration = DateTimeOffset.Now.AddMinutes(10);
            produtoRepository.SetItemProduto(parametros, expiration);
        }
    }
}
