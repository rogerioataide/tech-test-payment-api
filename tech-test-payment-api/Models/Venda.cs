﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using tech_test_payment_api.Enum;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key()]
        public string Id { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<int> ProdutoSequencial { get; set; }
        public DateTime DataVenda { get; set; }
        public string Usuario { get; set; }
        public eStatusVenda Status { get; set; }
    }
}
