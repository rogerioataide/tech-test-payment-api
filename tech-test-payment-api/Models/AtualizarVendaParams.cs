﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using tech_test_payment_api.Enum;

namespace tech_test_payment_api.Models
{
    public class AtualizarVendaParams
    {
        [Key()]
        public string Id { get; set; }
        public eStatusVenda Status { get; set; }
    }
}
