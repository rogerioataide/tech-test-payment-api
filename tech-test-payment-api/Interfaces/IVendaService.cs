﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Interfaces
{
    public interface IVendaService
    {
        object GetVenda(string id);
        void RegistrarVenda(RegistrarVendaParams registrarVendaParams);
        void AtualizarVenda(AtualizarVendaParams parametrosAtualizarVenda);
    }
}
