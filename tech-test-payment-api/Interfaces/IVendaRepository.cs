﻿using tech_test_payment_api.Models;

namespace tech_test_payment_api.Interfaces
{
    public interface IVendaRepository
    {
        void SetVenda(Venda venda);
        Venda GetVenda(string key);
        bool VendaExiste(string key);
    }
}
