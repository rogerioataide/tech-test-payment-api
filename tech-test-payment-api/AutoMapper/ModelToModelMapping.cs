﻿using AutoMapper;
using tech_test_payment_api.Enum;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.AutoMapper
{
    public class ModelToModelMapping : Profile
    {

        public ModelToModelMapping()
        {
            CreateMap<RegistrarVendaParams, Venda>()
                .ForMember(proc => proc.Id, reg => reg.MapFrom(x => x.Id))
                .ForMember(proc => proc.DataVenda, reg => reg.MapFrom(x => x.DataVenda))
                .ForMember(proc => proc.Usuario, reg => reg.MapFrom(x => x.Usuario))
                .ForMember(proc => proc.ProdutoSequencial, reg => reg.MapFrom(x => x.ProdutoSequencial))
                .ForMember(proc => proc.Status, reg => reg.MapFrom(x => eStatusVenda.AguardandoPagamento))
                .ForPath(proc => proc.Vendedor.Cpf, reg => reg.MapFrom(x => x.Vendedor.Cpf))
                .ForPath(proc => proc.Vendedor.Nome, reg => reg.MapFrom(x => x.Vendedor.Nome))
                .ForPath(proc => proc.Vendedor.Email, reg => reg.MapFrom(x => x.Vendedor.Email))
                .ForPath(proc => proc.Vendedor.Telefone, reg => reg.MapFrom(x => x.Vendedor.Telefone));
        }

    }
}
