﻿using System.Runtime.Caching;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Cache
{
    public class ProdutoRepository
    {
        private static readonly ObjectCache produto = MemoryCache.Default;

        public void SetItemProduto(Produto produto, DateTimeOffset absoluteExpiration)
        {
            CacheItemPolicy policy = new CacheItemPolicy
            {
                AbsoluteExpiration = absoluteExpiration
            };
            ProdutoRepository.produto.Set(produto.Id, produto, policy);
        }

        public object GetCacheProduto(string key)
        {
            return produto.Get(key);
        }

        public void RemoveCacheProduto(string key)
        {
            if (produto.Contains(key))
            {
                produto.Remove(key);
            }
        }
    }
}
