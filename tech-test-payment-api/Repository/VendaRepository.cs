﻿using tech_test_payment_api.Interfaces;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Exceptions;

namespace tech_test_payment_api.Cache
{
    public class VendaRepository : IVendaRepository
    {
        private static List<Venda> vendas = new List<Venda>();

        public void SetVenda(Venda venda)
        {
            vendas.Add(venda);
        }

        public Venda GetVenda(string key)
        {
            var venda = vendas.Where(a => a.Id == key).FirstOrDefault();

            if (venda != null)
            {
                return venda;
            }

            throw new VendaNaoEncontradaException($"Não foi encontrada nenhuma venda com o id {key}.");
        }

        public bool VendaExiste(string key)
        {
            return vendas.Exists(a => a.Id == key);
        }
    }
}
