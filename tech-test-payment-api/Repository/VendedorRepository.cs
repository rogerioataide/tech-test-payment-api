﻿using System.Runtime.Caching;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Cache
{
    public class VendedorRepository
    {
        private static readonly ObjectCache vendedor = MemoryCache.Default;

        public void SetItemVendedor(Vendedor vendedor, DateTimeOffset absoluteExpiration)
        {
            CacheItemPolicy policy = new CacheItemPolicy
            {
                AbsoluteExpiration = absoluteExpiration
            };
            //VendedorRepository.vendedor.Set(vendedor.Id, vendedor, policy);
        }

        public object GetCacheVendedor(string key)
        {
            return vendedor.Get(key);
        }

        public void RemoveCacheVendedor(string key)
        {
            if (vendedor.Contains(key))
            {
                vendedor.Remove(key);
            }
        }
    }
}
