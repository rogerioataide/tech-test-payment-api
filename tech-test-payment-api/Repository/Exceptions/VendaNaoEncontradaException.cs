﻿namespace tech_test_payment_api.Repository.Exceptions
{
    [Serializable]
    public class VendaNaoEncontradaException : Exception
    {
        public VendaNaoEncontradaException() { }

        public VendaNaoEncontradaException(string message)
            : base(message) { }

        public VendaNaoEncontradaException(string message, Exception inner)
            : base(message, inner) { }
    }
}
