﻿namespace tech_test_payment_api.Repository
{
    [Serializable]
    public class VendaJaCadastradaException : Exception
    {
        public VendaJaCadastradaException() { }

        public VendaJaCadastradaException(string message)
            : base(message) { }

        public VendaJaCadastradaException(string message, Exception inner)
            : base(message, inner) { }
    }
}
