﻿namespace tech_test_payment_api.Repository
{
    [Serializable]
    public class StatusVendaInvalidoException : Exception
    {
        public StatusVendaInvalidoException() { }

        public StatusVendaInvalidoException(string message)
            : base(message) { }

        public StatusVendaInvalidoException(string message, Exception inner)
            : base(message, inner) { }
    }
}
