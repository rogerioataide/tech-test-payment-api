﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using tech_test_payment_api.Cache;
using tech_test_payment_api.Interfaces;
using tech_test_payment_api.Models;
using tech_test_payment_api.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TechTestPaymentApiController.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TechTestPaymentApiController : ControllerBase
    {
        private readonly IVendaService _vendaService;

        public TechTestPaymentApiController(IVendaService vendaService)
        {
            _vendaService = vendaService;
        }

        /// <summary>
        /// Seleciona uma venda já realizada
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("SelecionarVendaPorId/{id}")]
        public async Task<IActionResult> SelecionarVendaPorId(string id)
        {
            var retorno = _vendaService.GetVenda(id);

            if (retorno != null)
            {
                return Ok(retorno);
            }

            return Ok("Venda não localizada!");
        }

        /// <summary>
        /// Registra uma venda com dados do produto e do vendedor
        /// </summary>
        /// <param name="parametros"></param>
        /// <exception cref="Exception"></exception>
        [HttpPost("RegistrarVenda/")]
        public void RegistrarVenda([FromBody] RegistrarVendaParams parametros)
        {
            if (parametros.ProdutoSequencial.Count < 1)
            {
                //TODO: implementar retorno apenas de mensagem de erro
                throw new Exception("A inclusão de uma venda deve possuir pelo menos 1 produto!");
            }

            _vendaService.RegistrarVenda(parametros);
        }

        /// <summary>
        /// Atualiza o status de uma venda
        /// </summary>
        /// <param name="parametrosAtualizarVenda">0) AguardandoPagamento, 1) PagamentoAprovado, 2) Cancelado, 3) EnviadoTransportadora, 4) Entregue</param>
        [HttpPut("AtualizarVenda/")]
        public void AtualizarVenda(AtualizarVendaParams parametrosAtualizarVenda)
        {
            _vendaService.AtualizarVenda(parametrosAtualizarVenda);
        }
    }
}
